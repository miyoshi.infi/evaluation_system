from flask import Flask, redirect, url_for, render_template, request, session
from datetime import timedelta
from functools import wraps
from distutils.util import strtobool

from ev_sys_pwa.app.utils.database.utils import get_func, get_evaluation_results, get_all_evaluation_elements, \
    get_input_number, get_user, create_input_number, create_ev_item_user, get_members, get_all_input_numbers, \
    get_ev_target_item, create_ev_target_item, create_summary, create_initial_evaluation_items, get_summary, \
    get_ev_item_user
from ev_sys_pwa.app.utils.database.data_class import User, EvaluationNumber, InputNumberStep, Authority
from ev_sys_pwa.app.settings.const import Message, HISTORY_GENERATIONS, ToolBarDisplayTarget

app = Flask(__name__)

app.secret_key = 'abcdefghijklmn'
app.permanent_session_lifetime = timedelta(days=1)


def session_check(func):
    @wraps(func)
    def wrapper():
        if "id" in session:
            return func()
        return render_template("index.html")

    return wrapper


@app.route("/", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        session.permanent = True
        id_ = request.form.get("id")
        password = request.form.get('pass')

        user = get_func(User, "username", id_)
        if user:
            if user.authenticated(password):
                session["id"] = user.username
                return redirect(url_for("login_"))
            else:
                return render_template("index.html")
        else:
            if id_ == "administrator":
                admin = User()
                admin.set_parameters(
                    username=id_,
                    disp_name="管理者",
                    password=password,
                    authority=Authority.Administrator.value,
                )
                admin.save()
                return render_template("init.html", name=admin.disp_name)
            return render_template("index.html")
    else:
        if "id" in session:
            return redirect(url_for("login_"))
        return render_template("index.html")


@app.route("/login", methods=["GET", "POST"])
@session_check
def login_():
    user = get_user(session["id"])
    if user.authority in [
        Authority.Administrator.value,
        Authority.Manager.value,
        Authority.Viewer.value,
    ]:
        return render_template("member_list.html",
                               id=session["id"],
                               name=user.disp_name,
                               members=get_members(),
                               toolbar=ToolBarDisplayTarget.get("member_list"),
                               )

    if user.authority in [Authority.Member.value]:
        return redirect(url_for("current_evaluations"))


@app.route("/init", methods=["POST"])
@session_check
def init():
    # データベース初期化処理
    create_initial_evaluation_items()
    return redirect(url_for("login_"))


@app.route("/current_evaluations", methods=["GET"])
@session_check
def current_evaluations():
    # ユーザ、対象メンバーの特定
    user = get_user(session["id"])
    if user.authority <= Authority.Viewer.value:
        target_member = request.args.get('target_member')
    else:
        target_member = session["id"]

    if not target_member:
        return redirect(url_for("login_"))

    latest_input_number = get_input_number(target_member)

    if latest_input_number:
        # 評価が入力済みの場合
        input_number = latest_input_number
        if latest_input_number.number in [
            InputNumberStep.user_input.value,
            InputNumberStep.manager_input.value,
        ]:
            all_numbers = set(get_all_input_numbers(target_member))
            if user.authority == Authority.Manager.value:
                # マネージャー
                if not all_numbers & {InputNumberStep.manager_input.value}:
                    # マネージャーの評価が未入力の場合
                    input_number = get_input_number(target_member, InputNumberStep.init.value)
            elif user.authority == Authority.Member.value:
                # メンバー
                if not all_numbers & {InputNumberStep.user_input.value}:
                    # メンバーの評価が未入力の場合
                    input_number = get_input_number(target_member, InputNumberStep.init.value)
            else:
                # マネージャー/メンバー以外
                input_number = get_input_number(target_member, InputNumberStep.init.value)
    else:
        # 評価が未入力の場合
        input_number = create_input_number(get_user(target_member), 0)

    # 画面表示内容の取得
    evaluation_results = get_evaluation_results(target_member, input_number)
    if input_number.number == InputNumberStep.manager_input.value:
        # マネージャー/メンバーの評価が入力済みの場合
        member_input = get_evaluation_results(target_member,
                                              get_input_number(target_member, InputNumberStep.user_input.value))
        for key in member_input:
            member_input[key]["evaluation_manager"] = evaluation_results.get(key, {}).get("evaluation",
                                                                                          EvaluationNumber.bad)
        evaluation_results = member_input

    # 保存ボタンの制御
    editable = False
    if user.authority == Authority.Member.value:
        if input_number.number == InputNumberStep.init.value:
            editable = True
    elif user.authority == Authority.Manager.value:
        if input_number.number < InputNumberStep.manager_input.value:
            editable = True
    updatable = editable
    if user.authority == Authority.Manager.value:
        updatable = True

    # 目標の取得
    ev_target_exist = False
    if input_number.number >= InputNumberStep.fixed.value:
        ev_target_items = get_ev_target_item(input_number)
        if ev_target_items:
            # 目標設定済み
            ev_target_exist = True
            for target_item in ev_target_items:
                evaluation_results.get(target_item.element.id, {})["target_exist"] = True
                evaluation_results.get(target_item.element.id, {})["target_detail"] = target_item.detail
        else:
            # 目標設定のため評価は設定不可
            editable = False

    ev_results_dict = {}
    role_descriptions = {}
    for key, data in evaluation_results.items():
        role_name = data.get("role", {}).get("disp_name")
        if role_name and role_name not in role_descriptions:
            role_descriptions[role_name] = data.get("role", {}).get("description")
        ev_results_dict.setdefault(role_name, {}).setdefault(
            data.get("section", {}).get("disp_name"), {}).update(
            {
                key: {
                    "description": data.get("description"),
                    "evaluation": data.get("evaluation", EvaluationNumber.bad.value),
                    "evaluation_manager": data.get("evaluation_manager", EvaluationNumber.bad.value),
                    "target_exist": data.get("target_exist", False),
                    "target_detail": data.get("target_detail"),
                }
            })

    # トップに表示するメッセージ
    message_args = {
        "target_member": get_user(target_member).disp_name,
    }
    message = Message().get_message(user.authority, input_number.number, ev_target_exist, **message_args)

    return render_template("current_evaluations.html",
                           user=user.convert_to_dict(),
                           target_member=target_member,
                           evaluations=ev_results_dict,
                           input_number=input_number.number,
                           role_descriptions=role_descriptions,
                           editable=editable,
                           updatable=updatable,
                           ev_target_exist=ev_target_exist,
                           message=message,
                           toolbar=ToolBarDisplayTarget.get("current_evaluations"),
                           )


@app.route("/update", methods=["POST"])
@session_check
def update():
    target_member = request.form.get("target_member")

    all_numbers = get_all_input_numbers(target_member)
    target_user = get_user(target_member)
    if target_member != session["id"]:
        user = get_user(session["id"])
        if user.authority != Authority.Manager.value:
            return redirect(url_for("login_"))

    update_eval = False
    if max(all_numbers) > InputNumberStep.manager_input.value:
        step = max(all_numbers)
        if strtobool(request.form.get("ev_target_exist", "False")):
            # 評価の更新の場合
            step += 1
            update_eval = True
    else:
        update_eval = True
        if len(set(all_numbers) & {InputNumberStep.user_input.value, InputNumberStep.manager_input.value}) == 2:
            step = InputNumberStep.fixed.value
        else:
            step = InputNumberStep.user_input.value if target_member == session["id"] else \
                InputNumberStep.manager_input.value

    eval_elements = get_all_evaluation_elements()
    if update_eval:
        # 評価の登録
        input_number = create_input_number(target_user, step)
        for form_key in request.form:
            if form_key.startswith("eval-"):
                if step == InputNumberStep.fixed.value:
                    if not form_key.endswith("-confirm"):
                        continue
                elif step > InputNumberStep.fixed.value:
                    if not form_key.endswith("-after"):
                        continue
                element_id = int(form_key.split("-")[1])
                element_key = dict(eval_elements.get(element_id, {})).get("key")
                evaluation = int(request.form.get(form_key))
                if element_key:
                    create_ev_item_user(input_number=input_number,
                                        element_key=element_key,
                                        evaluation=evaluation)
            if form_key.startswith("summary"):
                if step <= InputNumberStep.fixed.value:
                    continue
                create_summary(input_number=input_number,
                               summary=request.form.get(form_key))

    else:
        # 目標の登録
        latest_input_number = get_input_number(target_member)
        for form_key in request.form:
            if form_key.startswith("check-"):
                if strtobool(request.form.get(form_key)):
                    element_id = int(form_key.split("-")[1])
                    element_key = dict(eval_elements.get(element_id, {})).get("key")
                    target_text = request.form.get(f"target-{element_id}")
                    create_ev_target_item(latest_input_number, element_key, target_text)

    return redirect(url_for("current_evaluations"))


@app.route("/history", methods=["GET"])
@session_check
def history():
    user = get_user(session["id"])
    if user.authority <= Authority.Viewer.value:
        target_member = request.args.get('target_member')
    else:
        target_member = session["id"]
    all_numbers = get_all_input_numbers(target_member, raw=True)
    histories = []
    elements = get_all_evaluation_elements()
    for i in range(len(all_numbers) - 2, len(all_numbers) - (HISTORY_GENERATIONS + 1), -1):
        if i < InputNumberStep.fixed.value:
            break
        targets = {
            "number": all_numbers[i],
            "results": [],
            "summary": get_summary(all_numbers[i + 1]),
        }
        histories.append(targets)
        for target_item in get_ev_target_item(all_numbers[i]):
            element = elements.get(target_item.element.id)
            targets["results"].append({
                "element": element,
                "target_item": target_item,
                "before": get_ev_item_user(all_numbers[i].key, element["key"]),
                "after": get_ev_item_user(all_numbers[i + 1].key, element["key"]),
            })

    return render_template("history.html",
                           user=user.convert_to_dict(),
                           target_name=get_user(target_member).disp_name,
                           histories=histories,
                           toolbar=ToolBarDisplayTarget.get("history"),
                           )


@app.route("/settings", methods=["GET"])
@session_check
def settings():
    user = get_user(session["id"])
    return render_template("settings.html",
                           toolbar=ToolBarDisplayTarget.get("settings"),
                           user=user.convert_to_dict())


@app.route("/update_profile", methods=["POST"])
@session_check
def update_profile():
    user = get_user(session["id"])
    update_values = {
        "disp_name": request.form.get("profile_disp_name"),
    }
    if request.form.get("profile_password"):
        update_values["password"] = request.form.get("profile_password")
    user.set_parameters(**update_values)
    user.save()
    return redirect(url_for("settings"))


@app.route("/add", methods=["GET"])
@session_check
def add():
    user = get_user(session["id"])
    return render_template("add.html",
                           toolbar=ToolBarDisplayTarget.get("add"),
                           user_authority=list(Authority),
                           user=user.convert_to_dict())


@app.route("/add_user", methods=["POST"])
@session_check
def add_user():
    user_name = request.form.get("user_username")
    if get_user(user_name):
        return redirect(url_for("add"))
    user = User()
    print(request.form.get("user_authority", Authority.Member))
    user.set_parameters(
        username=user_name,
        disp_name=request.form.get("user_disp_name"),
        password=request.form.get("user_password"),
        authority=Authority[request.form.get("user_authority")].value,
    )
    user.save()
    return redirect(url_for("add"))


@app.route("/logout", methods=["GET"])
@session_check
def logout():
    session.pop('id', None)
    session.clear()
    return redirect("/")


if __name__ == "__main__":
    app.run(debug=True)
