from enum import Enum


# ユーザの有効/無効
class UserStatus(Enum):
    Active = 1
    Inactive = 0


# ユーザの権限
class Authority(Enum):
    Administrator = 0
    Manager = 10
    Viewer = 20
    Member = 30


# 評価
class EvaluationNumber(Enum):
    bad = 0
    good = 1
    great = 2


# 入力状況
# 0: 初期値
# 1: ユーザ初期評価入力済み
# 2: マネージャー初期評価入力済み
# 3: 評価合意済み
class InputNumberStep(Enum):
    init = 0
    user_input = 1
    manager_input = 2
    fixed = 3


# 画面に表示するメッセージテンプレート
class Message:
    DEFAULT = ""
    MEMBER_INIT = "まずは自己評価を入力しましょう。"
    MEMBER_WAIT_MANAGER = "上司の評価を待ちましょう。"
    MEMBER_ONE_ON_ONE_INITIAL = "上司と評価の認識合わせをしましょう。"
    MEMBER_ONE_ON_ONE_TARGET = "上司と目標を設定しましょう。"
    MEMBER_ONE_ON_ONE_RESULT = "上司と評価について話し合いましょう。"
    MANAGER_INIT = "{target_member}さんの評価をしましょう。"
    MANAGER_ONE_ON_ONE_INITIAL = "{target_member}さんと認識合わせをしましょう。"
    MANAGER_ONE_ON_ONE_TARGET = "{target_member}さんと目標を設定しましょう。"
    MANAGER_ONE_ON_ONE_RESULT = "{target_member}さんと評価について話し合いましょう。"

    MESSAGE_INIT = {
        Authority.Manager.value: {
            InputNumberStep.init.value: MANAGER_INIT,
            InputNumberStep.manager_input.value: MANAGER_ONE_ON_ONE_INITIAL,
        },
        Authority.Member.value: {
            InputNumberStep.init.value: MEMBER_INIT,
            InputNumberStep.user_input.value: MEMBER_WAIT_MANAGER,
            InputNumberStep.manager_input.value: MEMBER_ONE_ON_ONE_INITIAL,
        }
    }
    MESSAGE_TARGET = {
        Authority.Manager.value: {
            True: MANAGER_ONE_ON_ONE_RESULT,
            False: MANAGER_ONE_ON_ONE_TARGET,
        },
        Authority.Member.value: {
            True: MEMBER_ONE_ON_ONE_RESULT,
            False: MEMBER_ONE_ON_ONE_TARGET,
        }
    }

    def get_message(self, authority, input_number, target_exist, **kwargs):
        message = self.MESSAGE_INIT.get(authority, {}).get(input_number, self.DEFAULT).format(**kwargs)
        if message == self.DEFAULT:
            message = self.MESSAGE_TARGET.get(authority, {}).get(target_exist, self.DEFAULT).format(**kwargs)
        return message


# タイムゾーン
TIMEZONE = "Asia/Tokyo"

# 履歴を表示する世代数
HISTORY_GENERATIONS = 5


# ツールバーの項目
class ToolBarElements(Enum):
    return_ = "return"
    member_list = "member_list"
    history = "history"
    settings = "settings"
    logout = "logout"


# 画面毎の表示項目
ToolBarDisplayTargetMap = {
    "member_list": [
        ToolBarElements.settings,
        ToolBarElements.logout,
    ],
    "current_evaluations": [
        ToolBarElements.member_list,
        ToolBarElements.history,
        ToolBarElements.settings,
        ToolBarElements.logout,
    ],
    "settings": [
        ToolBarElements.return_,
        ToolBarElements.logout,
    ],
    "history": [
        ToolBarElements.settings,
        ToolBarElements.logout,
    ],
    "add": [
        ToolBarElements.return_,
        ToolBarElements.logout,
    ]
}

ToolBarDisplayTarget = {key: [x.value for x in value] for key, value in ToolBarDisplayTargetMap.items()}
