import hashlib
import os
import pytz
from datetime import datetime
from flask_login import UserMixin
from google.cloud import kms_v1

from datastore_entity import DatastoreEntity, EntityValue
from ev_sys_pwa.app.settings.const import *


class DatastoreKind(Enum):
    user = "ev_sys_user"
    project = "project"
    item_role = "ev_sys_evaluation_items_role"
    item_section = "ev_sys_evaluation_items_section"
    item_element = "ev_sys_evaluation_items_element"
    item_input_number = "ev_sys_evaluation_items_input_number"
    item_user = "ev_sys_evaluation_items_user"
    target_item = "ev_sys_evaluation_target_item"
    summary = "ev_sys_evaluation_summary"


class User(DatastoreEntity, UserMixin):
    __kind__ = DatastoreKind.user.value
    username = EntityValue(None)
    disp_name = EntityValue(None)
    password = EntityValue(None)
    password_raw = EntityValue(None)
    status = EntityValue(UserStatus.Active.value)
    authority = EntityValue(Authority.Member.value)
    projects = EntityValue([])

    def __init__(self, key=None, **kwargs):
        super().__init__(**kwargs)
        if key:
            self.key = key
            self.username = kwargs.get("username")
            self.disp_name = kwargs.get("disp_name", kwargs.get("username"))
            self.password = kwargs.get("password")
            self.password_raw = kwargs.get("password_raw")
            self.status = kwargs.get("status")
            self.authority = kwargs.get("authority")
            self.projects = kwargs.get("projects")

    def set_parameters(self, **kwargs):
        self.username = kwargs.get("username", self.username)
        self.disp_name = kwargs.get("disp_name", self.disp_name)
        password = kwargs.get("password")
        if password:
            self.password = hashlib.md5(password.encode()).hexdigest()
            client = kms_v1.KeyManagementServiceClient()
            key_name = client.crypto_key_path(
                os.getenv("PROJECT"),
                os.getenv("REGION"),
                "evaluation_system_ring",
                "user_auth",
            )
            request = kms_v1.EncryptRequest(name=key_name, plaintext=password.encode())
            response = client.encrypt(request=request)
            self.password_raw = response.ciphertext
        self.status = kwargs.get("status", self.status)
        self.authority = kwargs.get("authority", self.authority)
        self.projects = kwargs.get("projects", self.projects)

    def authenticated(self, password):
        hashed_pass = hashlib.md5(password.encode()).hexdigest()
        return True if hashed_pass == self.password else False

    def convert_to_dict(self):
        return self._convert_to_dict()


class Project(DatastoreEntity):
    __kind__ = DatastoreKind.project.value
    disp_name = EntityValue(None)

    def set_parameters(self, **kwargs):
        self.disp_name = kwargs.get("display")


class EvItemRole(DatastoreEntity):
    __kind__ = DatastoreKind.item_role.value
    disp_name = EntityValue(None)
    description = EntityValue(None)

    def set_parameters(self, **kwargs):
        self.disp_name = kwargs.get("display")
        self.description = kwargs.get("description")


class EvItemSection(DatastoreEntity):
    __kind__ = DatastoreKind.item_section.value
    disp_name = EntityValue(None)

    def set_parameters(self, **kwargs):
        self.disp_name = kwargs.get("display")


class EvItemElement(DatastoreEntity):
    __kind__ = DatastoreKind.item_element.value
    description = EntityValue(None)
    role = EntityValue(None)
    section = EntityValue(None)

    def set_parameters(self, **kwargs):
        self.description = kwargs.get("description")
        self.role = kwargs.get("role")
        self.section = kwargs.get("section")


class EvItemInputNumber(DatastoreEntity):
    __kind__ = DatastoreKind.item_input_number.value
    user = EntityValue(None)
    number = EntityValue(0)
    date = EntityValue(None)

    def __init__(self, key=None, **kwargs):
        super().__init__(**kwargs)
        if key:
            self.key = key
            self.user = kwargs.get("user")
            self.number = kwargs.get("number")
            if "date" in kwargs:
                self.date = kwargs.get("date").astimezone(pytz.timezone(TIMEZONE))

    def set_parameters(self, **kwargs):
        self.user = kwargs.get("user", self.user)
        self.number = kwargs.get("number", self.number)
        self.date = kwargs.get("date", datetime.now(pytz.utc))


class EvItemUser(DatastoreEntity):
    __kind__ = DatastoreKind.item_user.value
    input_number = EntityValue(None)
    element = EntityValue(None)
    evaluation = EntityValue(EvaluationNumber.bad.value)

    def __init__(self, key=None, **kwargs):
        super().__init__(**kwargs)
        if key:
            self.key = key
            self.input_number = kwargs.get("input_number")
            self.element = kwargs.get("element")
            self.evaluation = kwargs.get("evaluation")

    def set_parameters(self, **kwargs):
        self.input_number = kwargs.get("input_number", self.input_number)
        self.element = kwargs.get("element", self.element)
        self.evaluation = kwargs.get("evaluation", self.evaluation)


class EvTargetItem(DatastoreEntity):
    __kind__ = DatastoreKind.target_item.value
    input_number = EntityValue(None)
    element = EntityValue(None)
    detail = EntityValue(None)
    date = EntityValue(None)

    def __init__(self, key=None, **kwargs):
        super().__init__(**kwargs)
        if key:
            self.key = key
            self.input_number = kwargs.get("input_number")
            self.element = kwargs.get("element")
            self.detail = kwargs.get("detail")

    def set_parameters(self, **kwargs):
        self.input_number = kwargs.get("input_number", self.input_number)
        self.element = kwargs.get("element", self.element)
        self.detail = kwargs.get("detail", self.detail)
        self.date = kwargs.get("date", datetime.now(pytz.utc))


class EvSummary(DatastoreEntity):
    __kind__ = DatastoreKind.summary.value
    input_number = EntityValue(None)
    summary = EntityValue(None)
    date = EntityValue(None)

    def __init__(self, key=None, **kwargs):
        super().__init__(**kwargs)
        if key:
            self.key = key
            self.input_number = kwargs.get("input_number")
            self.summary = kwargs.get("summary")
            if "date" in kwargs:
                self.date = kwargs.get("date").astimezone(pytz.timezone(TIMEZONE))

    def set_parameters(self, **kwargs):
        self.input_number = kwargs.get("input_number", self.input_number)
        self.summary = kwargs.get("summary", self.summary)
        self.date = kwargs.get("date", datetime.now(pytz.utc))
